
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Manually_Upgrading_the_Kernel]]
= Manually Upgrading the Kernel
indexterm:[kernel,upgrading the kernel]indexterm:[kernel,package]indexterm:[kernel,RPM package]indexterm:[package,kernel RPM]
The {MAJOROS} kernel is custom-built by the {MAJOROS} kernel team to ensure its integrity and compatibility with supported hardware. Before a kernel is released, it must first pass a rigorous set of quality assurance tests.

{MAJOROS} kernels are packaged in the RPM format so that they are easy to upgrade and verify using the [application]*DNF* or [application]*PackageKit* package managers. [application]*PackageKit* automatically queries the DNF repositories and informs you of packages with available updates, including kernel packages.

This chapter is therefore *only* useful for users who need to manually update a kernel package using the [command]#rpm# command instead of [command]#dnf#.
indexterm:[kernel,installing kernel packages]indexterm:[installing the kernel]

.Use DNF to install kernels whenever possible
[WARNING]
====

Whenever possible, use either the [application]*DNF* or [application]*PackageKit* package manager to install a new kernel because they always *install* a new kernel instead of replacing the current one, which could potentially leave your system unable to boot.

====
indexterm:[kernel,installing kernel packages]
For more information on installing kernel packages with [application]*DNF*, see xref:package-management/DNF.adoc#sec-Updating_Packages[Updating Packages].

[[s1-kernel-packages]]
== Overview of Kernel Packages
indexterm:[kernel,kernel packages]indexterm:[kernel package,kernel,for single, multicore and multiprocessor systems]indexterm:[packages,kernel,for single, multicore and multiprocessor systems]indexterm:[kernel package,kernel-devel,kernel headers and makefiles]indexterm:[packages,kernel-devel,kernel headers and makefiles]indexterm:[kernel package,kernel-headers,C header files files]indexterm:[packages,kernel-headers,C header files files]indexterm:[kernel package,linux-firmware,firmware files]indexterm:[packages,linux-firmware,firmware files]indexterm:[kernel package,perf,firmware files]indexterm:[packages,perf,firmware files]
{MAJOROS} contains the following kernel packages:

* [package]*kernel* — Contains the kernel for single, multicore and multiprocessor systems.

* [package]*kernel-debug* — Contains a kernel with numerous debugging options enabled for kernel diagnosis, at the expense of reduced performance.

* [package]*kernel-devel* — Contains the kernel headers and makefiles sufficient to build modules against the [package]*kernel* package.

* [package]*kernel-debug-devel* — Contains the development version of the kernel with numerous debugging options enabled for kernel diagnosis, at the expense of reduced performance.

* [package]*kernel-headers* — Includes the C header files that specify the interface between the Linux kernel and user-space libraries and programs. The header files define structures and constants that are needed for building most standard programs.

* [package]*linux-firmware* — Contains all of the firmware files that are required by various devices to operate.

* [package]*perf* — This package contains supporting scripts and documentation for the [application]*perf* tool shipped in each kernel image subpackage.

* [package]*kernel-abi-whitelists* — Contains information pertaining to the {MAJOROS} kernel ABI, including a lists of kernel symbols that are needed by external Linux kernel modules and a [package]*dnf* plug-in to aid enforcement.

* [package]*kernel-tools* — Contains tools for manipulating the Linux kernel and supporting documentation.

[[s1-kernel-preparing]]
== Preparing to Upgrade
indexterm:[boot media]indexterm:[kernel,upgrading,preparing]indexterm:[kernel upgrading,preparing]indexterm:[kernel,upgrading,working boot media]
Before upgrading the kernel, it is recommended that you take some precautionary steps.

First, ensure that working boot media exists for the system in case a problem occurs. If the boot loader is not configured properly to boot the new kernel, you can use this media to boot into {MAJOROS}.

USB media often comes in the form of flash devices sometimes called _pen drives_, _thumb disks_, or _keys_, or as an externally-connected hard disk device. Almost all media of this type is formatted as a `VFAT` file system. You can create bootable USB media on media formatted as `ext2`, `ext3`, `ext4`, or `VFAT`.

You can transfer a distribution image file or a minimal boot media image file to USB media. Make sure that sufficient free space is available on the device. Around 4 GB is required for a distribution DVD image, around 700 MB for a distribution CD image, or around 10 MB for a minimal boot media image.

You must have a copy of the `boot.iso` file from a {MAJOROS} installation DVD, or installation CD-ROM#1, and you need a USB storage device formatted with the `VFAT` file system and around 16 MB of free space. The following procedure will not affect existing files on the USB storage device unless they have the same path names as the files that you copy onto it. To create USB boot media, perform the following commands as the `root` user:

. Install the [application]*SYSLINUX* bootloader on the USB storage device:
+
[subs="attributes"]
----
~]#{nbsp}syslinux /dev/sdX1
----
+
...where _sdX_ is the device name.

. Create mount points for `boot.iso` and the USB storage device:
+
[subs="attributes"]
----
~]#{nbsp}mkdir /mnt/isoboot /mnt/diskboot
----

. Mount `boot.iso`:
+
[subs="attributes"]
----
~]#{nbsp}mount -o loop boot.iso /mnt/isoboot
----

. Mount the USB storage device:
+
[subs="attributes"]
----
~]#{nbsp}mount /dev/sdX1 /mnt/diskboot
----

. Copy the [application]*ISOLINUX* files from the `boot.iso` to the USB storage device:
+
[subs="attributes"]
----
~]#{nbsp}cp /mnt/isoboot/isolinux/* /mnt/diskboot
----

. Use the `isolinux.cfg` file from `boot.iso` as the `syslinux.cfg` file for the USB device:
+
[subs="attributes"]
----
~]#{nbsp}grep -v local /mnt/isoboot/isolinux/isolinux.cfg > /mnt/diskboot/syslinux.cfg

----

. Unmount `boot.iso` and the USB storage device:
+
[subs="attributes"]
----
~]#{nbsp}umount /mnt/isoboot /mnt/diskboot

----

. You should reboot the machine with the boot media and verify that you are able to boot with it before continuing.

Alternatively, on systems with a floppy drive, you can create a boot diskette by installing the [package]*mkbootdisk* package and running the [command]#mkbootdisk# command as `root`. See [command]#man mkbootdisk# man page after installing the package for usage information.

To determine which kernel packages are installed, execute the command [command]#dnf list installed "kernel-*"# at a shell prompt. The output will comprise some or all of the following packages, depending on the system's architecture, and the version numbers might differ:

----
~]# dnf list installed "kernel-*"
Last metadata expiration check performed 0:28:51 ago on Tue May 26 21:22:39 2015.
Installed Packages
kernel-core.x86_64                      4.0.3-300.fc22                @System
kernel-core.x86_64                      4.0.4-300.fc22                @System
kernel-core.x86_64                      4.0.4-301.fc22                @System
kernel-headers.x86_64                   4.0.4-301.fc22                @System
kernel-modules.x86_64                   4.0.3-300.fc22                @System
kernel-modules.x86_64                   4.0.4-300.fc22                @System
kernel-modules.x86_64                   4.0.4-301.fc22                @System
----

From the output, determine which packages need to be downloaded for the kernel upgrade. For a single processor system, the only required package is the [package]*kernel* package. See xref:Manually_Upgrading_the_Kernel.adoc#s1-kernel-packages[Overview of Kernel Packages] for descriptions of the different packages.

[[s1-kernel-download]]
== Downloading the Upgraded Kernel
indexterm:[kernel,downloading]indexterm:[kernel,upgrade kernel available]indexterm:[kernel,upgrade kernel available,via Fedora Update System]indexterm:[kernel,upgrade kernel available,Security Advisories]
There are several ways to determine if an updated kernel is available for the system.

* Via Fedora Update System — Download and install the kernel RPM packages. For more information, refer to link:++https://bodhi.fedoraproject.org/++[].

* Via `_DNF_` using check-update:
----
dnf check-update --enablerepo=updates-testing
----

To install the kernel manually, continue to xref:Manually_Upgrading_the_Kernel.adoc#s1-kernel-perform-upgrade[Performing the Upgrade].

[[s1-kernel-perform-upgrade]]
== Performing the Upgrade
indexterm:[kernel,performing kernel upgrade]
After retrieving all of the necessary packages, it is time to upgrade the existing kernel.

.Keep the old kernel when performing the upgrade
[IMPORTANT]
====

It is strongly recommended that you keep the old kernel in case there are problems with the new kernel.

====

At a shell prompt, change to the directory that contains the kernel RPM packages. Use [option]`-i` argument with the [command]#rpm# command to keep the old kernel. Do *not* use the [option]`-U` option, since it overwrites the currently installed kernel, which creates boot loader problems. For example:

[subs="attributes"]
----
~]#{nbsp}rpm -ivh kernel-kernel_version.arch.rpm
----

The next step is to verify that the initial RAM disk image has been created. See xref:kernel-module-driver-configuration/Manually_Upgrading_the_Kernel.adoc#sec-Verifying_the_Initial_RAM_Disk_Image[Verifying the Initial RAM Disk Image] for details.

[[sec-Verifying_the_Initial_RAM_Disk_Image]]
== Verifying the Initial RAM Disk Image
indexterm:[initial RAM disk image,verifying]
The job of the initial RAM disk image is to preload the block device modules, such as for IDE, SCSI or RAID, so that the root file system, on which those modules normally reside, can then be accessed and mounted. On {MAJOROSVER} systems, whenever a new kernel is installed using either the [application]*DNF*, [application]*PackageKit*, or [application]*RPM* package manager, the [application]*Dracut* utility is always called by the installation scripts to create an _initramfs_ (initial RAM disk image).

On all architectures other than IBM eServer System i (see xref:Manually_Upgrading_the_Kernel.adoc#bh-Verifying_the_Initial_RAM_Disk_Image_and_Kernel_on_IBM_eServer_System_i[Verifying the Initial RAM Disk Image and Kernel on IBM eServer System i]), you can create an `initramfs` by running the [command]#dracut# command. However, you usually don't need to create an `initramfs` manually: this step is automatically performed if the kernel and its associated packages are installed or upgraded from RPM packages distributed by {OSORG}.

On architectures that use the GRUB 2 boot loader, you can verify that an `initramfs` corresponding to your current kernel version exists and is specified correctly in the `/boot/grub2/grub.cfg` configuration file by following this procedure:

[[procedure-Verifying_the_Initial_RAM_Disk_Image]]
.Verifying the Initial RAM Disk Image
. As `root`, list the contents in the `/boot/` directory and find the kernel (`vmlinuz-_kernel_version_pass:attributes[{blank}]`) and `initramfs-_kernel_version_pass:attributes[{blank}]` with the latest (most recent) version number:
+
[[ex-Ensuring_that_the_kernel_and_initramfs_versions_match]]
.Ensuring that the kernel and initramfs versions match
====

----
~]# ls /boot/
config-3.17.4-302.fc21.x86_64
config-3.17.6-300.fc21.x86_64
config-3.17.7-300.fc21.x86_64
efi
elf-memtest86+-5.01
extlinux
grub2
initramfs-0-rescue-db90b4e3715b42daa871351439343ca4.img
initramfs-3.17.4-302.fc21.x86_64.img
initramfs-3.17.6-300.fc21.x86_64.img
initramfs-3.17.7-300.fc21.x86_64.img
initrd-plymouth.img
lost+found
memtest86+-5.01
System.map-3.17.4-302.fc21.x86_64
System.map-3.17.6-300.fc21.x86_64
System.map-3.17.7-300.fc21.x86_64
vmlinuz-0-rescue-db90b4e3715b42daa871351439343ca4
vmlinuz-3.17.4-302.fc21.x86_64
vmlinuz-3.17.6-300.fc21.x86_64
vmlinuz-3.17.7-300.fc21.x86_64
----

====
+
xref:Manually_Upgrading_the_Kernel.adoc#ex-Ensuring_that_the_kernel_and_initramfs_versions_match[Ensuring that the kernel and initramfs versions match] shows that:
+
** we have three kernels installed (or, more correctly, three kernel files are present in the `/boot/` directory),
+
** the latest kernel is `vmlinuz-3.17.7-300.fc21.x86_64`, and
+
** an `initramfs` file matching our kernel version, `initramfs-3.17.7-300.fc21.x86_64.img`, also exists.
+
[[important-initrd_files_in_the__boot_directory_are_not_the_same_as_initramfs_files]]
.initrd files in the /boot/ directory are not the same as initramfs files
[IMPORTANT]
====

In the `/boot/` directory you might find several `initrd-_kernel_version_pass:attributes[{blank}]kdump.img` files. These are special files created by the `kdump` mechanism for kernel debugging purposes, are not used to boot the system, and can safely be ignored. For more information on `kdump`, see the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Kernel_Crash_Dump_Guide/++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Kernel Crash Dump Guide].

====

. (Optional) If your `initramfs-_kernel_version_pass:attributes[{blank}]` file does not match the version of the latest kernel in `/boot`, or, in certain other situations, you might need to generate an `initramfs` file with the [application]*Dracut* utility. Simply invoking [command]#dracut# as `root` without options causes it to generate an `initramfs` file in the `/boot/` directory for the latest kernel present in that directory:
+
----
~]# dracut
----
+
You must use the [option]`--force` option if you want [command]#dracut# to overwrite an existing `initramfs` (for example, if your `initramfs` has become corrupt). Otherwise [command]#dracut# will refuse to overwrite the existing `initramfs` file:
+
----
~]# dracut
F: Will not override existing initramfs (/boot/initramfs-3.17.7-300.fc21.x86_64.img) without --force
----
+
You can create an initramfs in the current directory by calling [command]#dracut _initramfs_name_ _kernel_version_pass:attributes[{blank}]#, for example:
+
----
~]# dracut "initramfs-$(uname -r).img" $(uname -r)
----
+
If you need to specify specific kernel modules to be preloaded, add the names of those modules (minus any file name suffixes such as `.ko`) inside the parentheses of the `add_dracutmodules="pass:attributes[{blank}]_module_ _more_modules_pass:attributes[{blank}]"` directive of the `/etc/dracut.conf` configuration file. You can list the file contents of an `initramfs` image file created by dracut by using the [command]#lsinitrd _initramfs_file_pass:attributes[{blank}]# command:
+
----
~]# lsinitrd /boot/initramfs-3.17.7-300.fc21.x86_64.img
Image: /boot/initramfs-3.17.7-300.fc21.x86_64.img: 18M
========================================================================
Version: dracut-038-31.git20141204.fc21
[output truncated]
----
+
See [command]#man dracut# and [command]#man dracut.conf# for more information on options and usage.

. Examine the `/boot/grub2/grub.cfg` configuration file to ensure that an `initramfs-_kernel_version_.img` file exists for the kernel version you are booting. For example:
+
----
~]# grep initramfs /boot/grub2/grub.cfg
	initrd16 /initramfs-3.17.7-300.fc21.x86_64.img
	initrd16 /initramfs-3.17.6-300.fc21.x86_64.img
	initrd16 /initramfs-3.17.4-302.fc21.x86_64.img
	initrd16 /initramfs-0-rescue-db90b4e3715b42daa871351439343ca4.img
----
+
See xref:Manually_Upgrading_the_Kernel.adoc#s1-kernel-boot-loader[Verifying the Boot Loader] for more information on how to read and update the `/boot/grub2/grub.cfg` file.

[[bh-Verifying_the_Initial_RAM_Disk_Image_and_Kernel_on_IBM_eServer_System_i]]
.Verifying the Initial RAM Disk Image and Kernel on IBM eServer System i

On IBM eServer System i machines, the initial RAM disk and kernel files are combined into a single file, which is created with the [command]#addRamDisk# command. This step is performed automatically if the kernel and its associated packages are installed or upgraded from the RPM packages distributed by {OSORG}; thus, it does not need to be executed manually. To verify that it was created, run the following command as `root` to make sure the `/boot/vmlinitrd-_kernel_version_pass:attributes[{blank}]` file already exists:

[subs="quotes, macros"]
----
[command]#ls -l /boot/#
----

The _kernel_version_ should match the version of the kernel just installed.

[[s1-kernel-boot-loader]]
== Verifying the Boot Loader
indexterm:[boot loader,verifying]
When you install a kernel using [command]#rpm#, the kernel package creates an entry in the boot loader configuration file for that new kernel. However, [command]#rpm# does *not* configure the new kernel to boot as the default kernel. You must do this manually when installing a new kernel with [command]#rpm#.

It is always recommended to double-check the boot loader configuration file after installing a new kernel with [command]#rpm# to ensure that the configuration is correct. Otherwise, the system might not be able to boot into {MAJOROS} properly. If this happens, boot the system with the boot media created earlier and re-configure the boot loader.

In the following table, find your system's architecture to determine the boot loader it uses, and then click on the "`See`" link to jump to the correct instructions for your system.

[[tb-grub-arch-loaders]]
.Boot loaders by architecture

[options="header"]
|===
|Architecture|Boot Loader|See
|x86|GRUB 2|xref:Manually_Upgrading_the_Kernel.adoc#s3-kernel-boot-loader-grub[Configuring the GRUB 2 Boot Loader]
|AMD AMD64 *or* Intel 64|GRUB 2|xref:Manually_Upgrading_the_Kernel.adoc#s3-kernel-boot-loader-grub[Configuring the GRUB 2 Boot Loader]
|IBM eServer System i|OS/400|xref:Manually_Upgrading_the_Kernel.adoc#s2-kernel-boot-loader-iseries[Configuring the OS/400 Boot Loader]
|IBM eServer System p|YABOOT|xref:Manually_Upgrading_the_Kernel.adoc#s2-kernel-boot-loader-pseries[Configuring the YABOOT Boot Loader]
|IBM System z|z/IPL|&mdash;
|===

[[s3-kernel-boot-loader-grub]]
=== Configuring the GRUB 2 Boot Loader
indexterm:[GRUB 2 boot loader,configuring]indexterm:[GRUB 2 boot loader,configuration file]
{MAJOROSVER} is distributed with GRUB 2, which reads its configuration from the `/boot/grub2/grub.cfg` file. This file is generated by the [application]*grub2-mkconfig* utility based on Linux kernels located in the `/boot` directory, template files located in `/etc/grub.d/`, and custom settings in the `/etc/default/grub` file and is automatically updated each time you install a new kernel from an RPM package. To update this configuration file manually, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#grub2-mkconfig# [option]`-o` [option]`/boot/grub2/grub.cfg`
----

Among various code snippets and directives, the `/boot/grub2/grub.cfg` configuration file contains one or more `menuentry` blocks, each representing a single GRUB 2 boot menu entry. These blocks always start with the `menuentry` keyword followed by a title, list of options, and opening curly bracket, and end with a closing curly bracket. Anything between the opening and closing bracket should be indented. For example, the following is a sample `menuentry` block for Fedora 21 with Linux kernel 3.17.6-300.fc21.x86_64:

----
menuentry 'Fedora (3.17.6-300.fc21.x86_64) 21 (Twenty One)' --class fedora --class gnu-linux --class gnu --class os --unrestricted $menuentry_id_option 'gnulinux-3.17.4-301.fc21.x86_64-advanced-effee860-8d55-4e4a-995e-b4c88f9ac9f0' {
        load_video
        set gfxpayload=keep
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        if [ x$feature_platform_search_hint = xy ]; then
          search --no-floppy --fs-uuid --set=root --hint='hd0,msdos1'  f19c92f4-9ead-4207-b46a-723b7a2c51c8
        else
          search --no-floppy --fs-uuid --set=root f19c92f4-9ead-4207-b46a-723b7a2c51c8
        fi
        linux16 /vmlinuz-3.17.6-300.fc21.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/swap rd.lvm.lv=fedora/root rhgb quiet LANG=en_US.UTF-8
        initrd16 /initramfs-3.17.6-300.fc21.x86_64.img
}
----

Each `menuentry` block that represents an installed Linux kernel contains `linux` and `initrd` directives followed by the path to the kernel and the `initramfs` image respectively. If a separate `/boot` partition was created, the paths to the kernel and the `initramfs` image are relative to `/boot`. In the example above, the `initrd16 /initramfs-3.17.6-300.fc21.x86_64.img` line means that the `initramfs` image is actually located at `/boot/initramfs-3.17.6-300.fc21.x86_64.img` when the root file system is mounted, and likewise for the kernel path.

The kernel version number as given on the `linux /vmlinuz-_kernel_version_pass:attributes[{blank}]` line must match the version number of the `initramfs` image given on the `initrd /initramfs-_kernel_version_.img` line of each `menuentry` block. For more information on how to verify the initial RAM disk image, refer to xref:Manually_Upgrading_the_Kernel.adoc#procedure-Verifying_the_Initial_RAM_Disk_Image[Verifying the Initial RAM Disk Image].

[[note-The_initrd_directive_in_grub.cfg_refers_to_an_initramfs_image]]
.The initrd directive in grub.cfg refers to an initramfs image
[NOTE]
====

In `menuentry` blocks, the `initrd` directive must point to the location (relative to the `/boot` directory if it is on a separate partition) of the `initramfs` file corresponding to the same kernel version. This directive is called `initrd` because the previous tool which created initial RAM disk images, [command]#mkinitrd#, created what were known as `initrd` files. The `grub.cfg` directive remains `initrd` to maintain compatibility with other tools. The file-naming convention of systems using the [command]#dracut# utility to create the initial RAM disk image is `initramfs-_kernel_version_.img`.

For information on using [application]*Dracut*, refer to xref:kernel-module-driver-configuration/Manually_Upgrading_the_Kernel.adoc#sec-Verifying_the_Initial_RAM_Disk_Image[Verifying the Initial RAM Disk Image].

====

After installing a new kernel with [command]#rpm#, verify that `/boot/grub2/grub.cfg` is correct and reboot the computer into the new kernel. Ensure your hardware is detected by watching the boot process output. If GRUB 2 presents an error and is unable to boot into the new kernel, it is often easiest to try to boot into an alternative or older kernel so that you can fix the problem. Alternatively, use the boot media you created earlier to boot the system.

.Causing the GRUB 2 boot menu to display
[IMPORTANT]
====

If you set the [option]`GRUB_TIMEOUT` option in the `/etc/default/grub` file to 0, GRUB 2 will not display its list of bootable kernels when the system starts up. In order to display this list when booting, press and hold any alphanumeric key while and immediately after BIOS information is displayed, and GRUB 2 will present you with the GRUB menu.

====

[[s2-kernel-boot-loader-iseries]]
=== Configuring the OS/400 Boot Loader
indexterm:[OS/400 boot loader,configuring]indexterm:[OS/400 boot loader,configuration file]
The `/boot/vmlinitrd-_kernel-version_pass:attributes[{blank}]` file is installed when you upgrade the kernel. However, you must use the [command]#dd# command to configure the system to boot the new kernel.

. As `root`, issue the command [command]#cat /proc/iSeries/mf/side# to determine the default side (either A, B, or C).

. As `root`, issue the following command, where _kernel-version_ is the version of the new kernel and _side_ is the side from the previous command:
+
[subs="quotes, macros"]
----
[command]#dd if=/boot/vmlinitrd-_kernel-version_ of=/proc/iSeries/mf/pass:attributes[{blank}]_side_pass:attributes[{blank}]/vmlinux bs=8k#
----

Begin testing the new kernel by rebooting the computer and watching the messages to ensure that the hardware is detected properly.

[[s2-kernel-boot-loader-pseries]]
=== Configuring the YABOOT Boot Loader

IBM eServer System p uses YABOOT as its boot loader. YABOOT uses `/etc/aboot.conf` as its configuration file. Confirm that the file contains an `image` section with the same version as the [package]*kernel* package just installed, and likewise for the `initramfs` image:

[subs="quotes, attributes"]
----
boot=/dev/sda1 init-message=Welcome to {MAJOROS}! Hit &lt;TAB&gt; for boot options
partition=2 timeout=30 install=/usr/lib/yaboot/yaboot delay=10 nonvram
image=/vmlinuz-2.6.32-17.EL
	 label=old
	 read-only
	 initrd=/initramfs-2.6.32-17.EL.img
	 append="root=LABEL=/"
image=/vmlinuz-2.6.32-19.EL
	 label=linux
	 read-only
	 initrd=/initramfs-2.6.32-19.EL.img
	 append="root=LABEL=/"
----

Notice that the default is not set to the new kernel. The kernel in the first image is booted by default. To change the default kernel to boot either move its image stanza so that it is the first one listed or add the directive `default` and set it to the `label` of the image stanza that contains the new kernel.

Begin testing the new kernel by rebooting the computer and watching the messages to ensure that the hardware is detected properly.
